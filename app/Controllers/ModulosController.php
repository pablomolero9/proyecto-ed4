<?php

    //Ésta es la carpeta dónde se almacena el nuevo Controlador.
    namespace App\Controllers;

    //Aqui lo seleccionamos para enlazarlo con el Modelo.
    use App\Models\ModulosModel;

    /* Después añadimos la clase con el nuevo controlador
    y lo enlazamos con la base de datos 'centro' y a su vez la tabla 'alumnos'.
    También se tiene que enlazar a su vez con el Modelo y la Vista que hemos creado. */
    class ModulosController extends BaseController {
        public function index() 
        {
           $grupos = new ModulosModel();
           $lista ['grupos'] = $grupos->findAll();
           echo view('alumnos/ModulosVista', $lista);
        }
    }
             
 
    